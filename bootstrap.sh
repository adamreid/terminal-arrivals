#!/bin/sh

bootstrap_redhat() {
  sudo yum install -y git
}

init() {
  if [ -z $TA_CLONE_URL ]; then
    echo "Please set the TA_CLONE_URL to the git repo you are runing terminal-arrivals from"
  fi
  mkdir -p $HOME/.local/
  git clone $TA_CLONE_URL $HOME/.local/terminal-arrivals/
}

if [ -e /etc/redhat-release ]; then
  echo "Setting up for RedHat based linux."
  bootstrap_redhat
  init
else
  echo "Terminal-Arrivals only supports RedHat at the moment."
fi


